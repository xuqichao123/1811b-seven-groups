export default {
  state: () => ({
    classList: [],
    // 全选
    checkAll: false,
    newClass: [],
  }),
  actions: {},
  mutations: {
    // 获取数据
    GET_CLASS_LIST(state, data) {
      state.classList = data;
    },
    // 全选
    CHANGE_FLAG_ALL(state) {
      state.checkAll = !state.checkAll;
      state.classList.forEach((item) => {
        item.flag = state.checkAll;
      });
      console.log(state.classList);
    },
    // 单选
    CHANGE_FLAG(state, id) {
      state.classList.forEach((item) => {
        if (item.id === id) {
          item.flag = !item.flag;
        }
      });
      state.checkAll = state.classList.every((item) => item.flag);
    },
    // 添加小组
    ADD_CLICK(state) {
      state.newClass.push([]);
    },
    // 添加成员
    ADD_MEMBERS(state, i) {
      state.classList.forEach((item) => {
        if (item.flag) {
          state.newClass[i].push(item);
        }
      });
      state.classList = state.classList.filter((item) => !item.flag);
    },
    // 删除成员
    DEL_MEMBERS(state, { i1, i2 }) {
      let item = state.newClass[i1][i2];
      // console.log(i1, i2, item);
      item.flag = false;
      state.newClass[i1].splice(i2, 1);
      state.classList.unshift(item);
    },
    // 删除小组
    DEL_GROUP(state, i) {
      state.newClass[i].forEach((item) => {
        item.flag = false;
        state.classList.unshift(item);
      });
      state.newClass.splice(i, 1);
    },
    // 取消
    CANCEL(state) {
      state.newClass = [];
    },
    // 自动分组
    AUTO_GROUP(state, num) {
      console.log(state, num);
      // const sum = Math.floor(state.classList.length / num);
      // let data = state.classList;
      // let arr;
      for (let i = 1; i <= num; i++) {
        state.newClass.push(
          state.classList.splice(0, i + state.classList.length / num)
        );
        // arr = data.splice(0, sum + (i === 1 ? state.classList.length % num : 10));
      }
      // let arr = state.classList.splice(0, sum);

      // console.log(JSON.stringify(state.newClass));
    },
    // 重置
    RESET(state) {
      state.newClass = [];
    },
  },
};
