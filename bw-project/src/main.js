import { createApp } from "vue";
import App from "./App.vue";
import { usePlugins } from "@/plugins";
import "./assets/tailwindcss.css";
import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";

// 面包屑全局组件
import Breadcrumb from "./components/Breadcrumb.vue";
// 超级表格
import SuperTable from "./components/SuperTable.vue";
// 鲁大师
import lodash from "lodash";

const app = createApp(App);
app.config.globalProperties.$_ = lodash;

usePlugins(app);
app.mount("#app");
app.use(ElementPlus);
app.component("breadcrumb", Breadcrumb);
app.component("super-table", SuperTable);
