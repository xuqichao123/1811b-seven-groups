export const interview = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};

export const interId = {
  url: "/dev-api/sypt/interview/deleteInterviewById/:id",
  method: "get",
};

export const interList2 = {
  url: "/dev-api/sypt/interview/interviewList",
  method: "get",
};
export const rangKing = {
  url: "/dev-api/sypt/interview/interviewRecordRangking",
  method: "get",
};

export const aRangKing = {
  url: "/dev-api/sypt/interview/interviewAnswerRangking",
  method: "get",
};

export const addInterview = {
  url: "/dev-api/sypt/interview",
  method: "post",
};
