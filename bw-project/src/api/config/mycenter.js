export const profile = {
  url: "/dev-api/system/user/profile",
  method: "get",
};

export const putProfile = {
  url: "/dev-api/system/user/profile",
  method: "put",
};

export const upDatePassword = {
  url: "/dev-api/system/user/profile/updatePwd",
  method: "put",
};
