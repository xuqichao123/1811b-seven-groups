//项目页面 项目列表全部数据
export const projectLists = {
  url: "/dev-api/sxpt/project/selectProjectList",
  method: "get",
};
//项目页面  获取全部专业接口
export const getMajorStationList = {
  url: "/dev-api/sxpt/label/selectMajorStationList",
  method: "get",
};
//项目页面 获取全部专业接口
export const getTradeList = {
  url: "/dev-api/sxpt/label/selectTradeList",
  method: "get",
};
// 实训大纲的Nav导航接口
export const selectBriefTree = {
  url: "/dev-api/sxpt/brief/selectBriefTree",
  method: "get",
};

export const selectProjectByVersionId = {
  url: "/dev-api/sxpt/project/selectProjectByVserionId",
  method: "get",
};

export const selectBriefTreeById = {
  url: "/dev-api/sxpt/brief/selectBriefTreeById",
  method: "get",
};
export const selectTaskList = {
  url: "/dev-api/sxpt/task/selectTaskList/8068389235944562688",
  method: "get",
};

// 实训环境中的接口
export const selectExperimentalList = {
  url: "/dev-api/sxpt/projectExperimental/selectExperimentalList",
  method: "get",
};

//前置项目中的接口
export const selectPushProjectByprojectVersionId = {
  url: "/dev-api/sxpt/frontPositionProject/selectPushProjectByprojectVersionId",
  method: "get",
};

//教师信息
export const teacherInfo = {
  url: "/dev-api/sypt/project/teacherInfo/8060404647629651968",
  method: "get",
};
//评论列表
export const getCommentList = {
  url: "/dev-api/sypt/discuss/list",
  method: "get",
};
//添加 评论接口
export const addComment = {
  url: "/dev-api/sypt/discuss",
  method: "post",
};

/***/ /////////////////////////////// */

//项目页面 项目列表全部数据
export const projectList = {
  url: "/dev-api/sypt/project/list",
  method: "get",
};
//项目页 模糊搜索

export const fuzzySearch = {
  url: "/dev-api/sypt/project/list",
  method: "get",
};
//  -------------------------------------------------------------------------

//实训任务请求
export const selectTaskLists = {
  //学生端
  url: "/dev-api/sxpt/task/selectTaskList/8060404647814201344",
  method: "get",
};
//
export const selectResourceTree = {
  url: "/dev-api/sxpt/projectResource/selectResourceTree",
  method: "get",
};
// 前置项目接口
export const selectProject = {
  url: "/dev-api/sxpt/frontPositionProject/selectPushProjectByprojectVersionId",
  method: "get",
};
