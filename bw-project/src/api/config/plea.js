//获取问答列表数据
export const getPleaTableData = {
  url: "/dev-api/sxpt/defence/getDefenceList?pageNum=1&pageSize=10&searchTitle=&defenceMjorId=&defenceStatus=",
  method: "get",
};

export const getInterViewRangking = {
  url: "/dev-api/sxpt/interview/interviewRecordRangkingTeacher?classId=",
  method: "get",
};

export const getInterViewRangkingT = {
  url: "dev-api/sxpt/interview/interviewAnswerRangkingTeacher?pageNum=1&pageSize=10&classId=",
  method: "get",
};
