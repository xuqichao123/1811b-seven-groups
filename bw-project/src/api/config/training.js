// 实训接口

// 收藏项目
export const collectProject = {
  url: "/dev-api/sypt/project/favor/list",
  method: "get",
};

// 相关实训
export const relatedTraining = {
  url: "/dev-api/sypt/project/list",
  method: "get",
};

// 切换状态
export const selectClassPlanByStu = {
  url: "/dev-api/sxpt/progress/selectClassPlanByStu",
  method: "get",
};

// 展开数据
export const selectClassPlanByStuInfo = {
  url: `/dev-api/sxpt/progress/selectClassPlanByStuInfo/:id`,
  method: "get",
};

//  ****** 详情 ******
// 行业
export const getTradeList = {
  url: "/dev-api/sxpt/label/selectTradeList",
  method: "get",
};

// 专业
export const getMajorStationList = {
  url: "/dev-api/sxpt/label/selectMajorStationList",
  method: "get",
};

// 分页数据
export const getPaginList = {
  url: "/dev-api/sypt/project/list",
  method: "get",
};

// ********项目详情页面*******

//获取tab切换的值
export const tabs = {
  url: "/dev-api/sxpt/brief/selectBriefTree",
  method: "get",
};

//获取评论数据
export const pinglun = {
  url: "/dev-api/sypt/discuss/list",
  method: "get",
};

//获取发表评论
export const comments = {
  url: "/dev-api/sypt/discuss",
  method: "post",
};

// 实训问答
export const question = {
  url: "/dev-api/sypt/answer/list/project",
  method: "get",
};

// 老师端******************************************************************

// 班级数据
export const getClassInfo = {
  url: "/dev-api/sxpt/classPlan/getClassInfo",
  method: "get",
};

// 切换班级
export const getPlanList = {
  url: "/dev-api/sxpt/classPlan/getPlanList",
  method: "get",
};

// 实训计划 *******************************
// 添加计划

// 选择班级获取班级人数
export const getClassStudent = {
  url: `/dev-api/sxpt/classPlan/getClassStudent/:id`,
  method: "get",
};

// 添加项目
export const getProjectList = {
  url: "/dev-api/sxpt/classPlan/getProjectList/",
  method: "get",
};

// 实训进度******************************

// 获取下拉框数据
export const selectClassPlan = {
  url: "/dev-api/sxpt/progress/selectClassPlan",
  method: "get",
};

// 右侧班级排行榜
export const classRank = {
  url: "/dev-api/sxpt/progress/classRank",
  method: "get",
};

// 班级小组信息
export const selectClassPlanInit = {
  url: "/dev-api/sxpt/progress/selectClassPlanInit",
  method: "get",
};

// 计划管理******************************
export const getPlanListAll = {
  url: "/dev-api/sxpt/classPlan/getPlanListAll",
  method: "get",
};

export const selectClassPlanInitAll = {
  url: "/dev-api/sxpt/progress/selectClassPlanInitAll",
  method: "get",
};
