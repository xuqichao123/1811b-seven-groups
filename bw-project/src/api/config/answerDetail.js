export const getDetail = {
  url: "/dev-api/sypt/answer:answerId",
  method: "get",
};
export const goDetail = {
  url: "/dev-api/sypt/reply/answerList",
  method: "get",
};
export const getReply = {
  url: "/dev-api/sypt/answer/reply",
  method: "get",
};
export const hiddenChangeTrue = {
  url: "/dev-api/sypt/collection/add",
  method: "get",
};
export const hiddenChangeFalse = {
  url: "/dev-api/sypt/collection/delete",
  method: "get",
};
export const setReply = {
  url: "/dev-api/sypt/answer/reply",
  method: "post",
};
