//获取问答列表数据
export const reply = {
  url: "/dev-api/sypt/answer/reply",
  method: "get",
};
//获取我的问题列表数据
export const getAsk = {
  url: "/dev-api/sypt/answer/myAsk",
  method: "get",
};
//获取我的回答列表数据
export const getAnswer = {
  url: "/dev-api/sypt/answer/myAnswer",
  method: "get",
};
//删除
export const del = {
  url: "/dev-api/sypt/answer/delete",
  method: "delete",
};
//修改
export const edit = {
  url: "/dev-api/sypt/answer/selectContextByAnswerId",
  method: "get",
};
