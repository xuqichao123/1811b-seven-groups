// 岗位接口

// 获取专业数据
export const selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};

// 获取表格数据、赛选、搜索
export const selectStationVersionList = {
  url: "/dev-api/sypt/station/selectStationNewVersionList",
  method: "get",
};

// 查看岗位
export const selectStationListById = {
  url: "/dev-api/sxpt/station/selectStationListById/:id",
  method: "get",
};

// 老师端*******************************

// 表格数据
export const getList = {
  url: "/dev-api/sxpt/station/selectStationVersionList",
  method: "get",
};

// 保存
export const station = {
  url: "/dev-api/sxpt/station",
  method: "post",
};

// 添加岗位
export const inintSkill = {
  url: "/dev-api/sxpt/skill/inintSkill",
  method: "post",
};

// 搜索技能岗位
export const selectSkillList = {
  url: "/dev-api/sxpt/skill/selectSkillList",
  method: "get",
};

// 岗位能力保存
export const skill = {
  url: "/dev-api/sxpt/skill",
  method: "post",
};

// 提交审核    stationVersionId
export const updateStationStatus = {
  url: "/dev-api/sxpt/station/updateStationStatus",
  method: "get",
};

// 撤销
export const revocation = {
  url: "/dev-api/sxpt/station/updateStationStatusCancel",
  method: "get",
};

// 删除
export const deleteStation = {
  url: "/dev-api/sxpt/station/deleteStation",
  method: "DELETE",
};

// 编辑页查询技能
export const selectSkillInfo = {
  url: "/dev-api/sxpt/skill/selectSkillInfo/:id",
  method: "get",
};
